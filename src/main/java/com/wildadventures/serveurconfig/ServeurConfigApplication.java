package com.wildadventures.serveurconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServeurConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServeurConfigApplication.class, args);
	}
}
